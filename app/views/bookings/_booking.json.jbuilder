json.extract! booking, :id, :name, :date, :days, :peoples, :room_type, :phone, :created_at, :updated_at
json.url booking_url(booking, format: :json)
