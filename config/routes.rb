Rails.application.routes.draw do
  get 'about/index'
  root to: 'home#index'
  get 'roomtype/index'
  get 'roomtype/room1'
  get 'roomtype/room2'
  get 'roomtype/room3'
  get 'roomtype/room4'
  get 'roomtype/room5'
  get 'roomtype/room6'
  get 'bookings/index'
  resources :bookings
  # For details on the DSL available within this file, see https://guides.rubyonrails.org/routing.html
end
